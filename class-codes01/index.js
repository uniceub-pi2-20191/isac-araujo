
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

import Login from './src/components/Login/Login';

AppRegistry.registerComponent(appName, () => Login);