import React, {Component} from 'react';
import {View, Image, StyleSheet, TextInput} from 'react-native';


export default class Login extends Component {
	constructor(props) {
    	super(props);
    	this.state = {text: ''};
  	}
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.inner}>
					<Image style={styles.image} source={require('../images/logo.jpeg')}/>
					<TextInput
          				style={{height: 40, borderWidth: 1, width: 250 }}
          				placeholder="Login"
          				onChangeText={(text) => this.setState({text})}
          			/>
          			<TextInput
          				style={{height: 40, borderWidth: 1, width: 250}}
          				placeholder="Password"
          				onChangeText={(text) => this.setState({text})}
          			/>
				</View>
			</View>

	);}
}


const styles = StyleSheet.create({
	container: {
	    borderRadius: 4,
	    borderWidth: 0.5,
	    backgroundColor: '#519e8a',
	    height: '100%'

	},
	inner: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center' 
	},

	image: {
	    width: 200,
	    height: 200
  	}
});
